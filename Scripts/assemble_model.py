import numpy as np
from stl import mesh
import matplotlib.pyplot as plt
import pandas as pd
from os import listdir
import math 
import matplotlib.pyplot as plt 

maxZ = 0
distanceBetweenLayers = 23.8

def pol2cartWithMaxZ(phi, rho, z):
    global maxZ
    z = z*distanceBetweenLayers
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    if z > maxZ:
        maxZ = z
    return(x, y, z)

files = listdir('./')
df = pd.read_csv('./'+files[0],header = 0, index_col=0)
columns = df.columns

'''Every 0,2,4..n-1 column has a roundness-value as a label and radius-values
as data.'''

roundness_idx = [2*i for i in range(len(columns))]
roundness_columns = [float(i) for i in columns[0:len(columns):2]]
theta_columns = columns[1:len(columns):2]

coords = []
lengthRestriction = 25
verticesInSlice = 256
radiusOffset = 80


for i in range(0, len(columns), 2):
    j = int(i/2)
    if j  == lengthRestriction:
        break

    radius = df[str(roundness_columns[j])].to_numpy()
    radius = radius.reshape(-1)
    thetas = df[theta_columns[j]].to_numpy() # angle in the range 0 - 2*Pi

    for k in range (0, verticesInSlice):
        coords.append(pol2cartWithMaxZ(thetas[k],radius[k] + radiusOffset, (i/2)+ 1))

#-----------------------------------------------------------------------------------
# First draw the top polygons

faces = []
for i in range(1, verticesInSlice-1):
    faces.append((i, i+1, 0))   #This is top faces

#-----------------------------------------------------------------------------------
# First polygon row
for i in range(1, verticesInSlice-1):
    faces.append((i, i+255, i+256))
    faces.append((i, i+256, i+1))

#--------------------------------------------------------------
# Main body	
for k in range(1, lengthRestriction-1):
    for i in range(k*verticesInSlice, ((k+1)*verticesInSlice)-1):
        faces.append((i, i+255, i+256))
        faces.append((i, i+256, i+1))
    faces.append((verticesInSlice*k+255, verticesInSlice*k, verticesInSlice*(k-1)+255))
    faces.append((verticesInSlice*k+255,verticesInSlice*(k-1)+255, verticesInSlice*k+254))

#---------------------------------------------------------------
# The bottom
#for i in range((lengthRestriction-2)*verticesInSlice, (lengthRestriction-1)*verticesInSlice - 1):
 #   faces.append((i-1, i, maxZ))   #This is top faces


verticesNP = np.array(coords[0:verticesInSlice*lengthRestriction])
facesNP = np.array(faces)

#print(verticesNP)

	
	# Create the mesh
assembly = mesh.Mesh(np.zeros(facesNP.shape[0], dtype=mesh.Mesh.dtype))
for i, f in enumerate(facesNP):
    for j in range(3):
        assembly.vectors[i][j] = verticesNP[f[j],:]

# Write the mesh to file
assembly.save('assembly.stl')








	